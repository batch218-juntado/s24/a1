// console.log("Hello world");

/*
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
*/

/* [Exponent Operator and Template Literals]
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
*/

const getCube = (x, y) =>{
	console.log(`The cube of ${x} is ${x**y}`)
};

getCube(2,3);

/* [Array Destructuring and Template Literals]
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/
let fullAddress = ["Flughafenstrasse", "Ensdorf", "Germany"];


let [street, city, country] = fullAddress;
console.log(`I live in ${street} at ${city} City, ${country}`);

/* [Object Destructuring and Template Literals]
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

let animal={
	name: "Panda",
	sciName: "Ailuropoda melanoleuca",
	aclass: "mammalia",
	family: "ursidae",
};

let {name, sciName, aclass, family} = animal;
console.log(`The ${name} is characterised by its bold black-and-white coat. Its scientific name is ${sciName}. Its class is ${aclass} and belongs to the family ${family}.`);

/* [forEach() method and Implicit Return(arrow function => )]
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/

let numbers = [1, 2, 3, 4, 5];

numbers.forEach(num => console.log(num));

/* [reduce() method and Implicit Return(arrow function => )]
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers from array of numbers of instuction #9.
*/

let sumNumbers = numbers.reduce((acc, curr) => acc + curr);

console.log(sumNumbers);


/*  [Class-Based Object Blueprint / JavaScript Classes]
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let frankie = new Dog("Frankie", 5, "Miniature Daschund");

console.log(frankie);


// 14. Create a git repository named S24.

